@extends('theme')

@section('title') Edición de contacto {{$contact->name}} @stop

@section('header_title') Editando perfil de {{$contact->name}} {{$contact->lastname}} @stop

@section('content')
	{{Form::open(array('route' => array('contacts.update', $contact->id), 'method' => 'PUT'))}}
	@include('contacts._form')
	{{Form::close()}}
@stop