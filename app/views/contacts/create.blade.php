@extends('theme')

@section('title') Nuevo Contacto @stop

@section('header_title') Nuevo Contacto @stop

@section('content')
	{{Form::open(array('route' => 'contacts.store', 'method' => 'POST'))}}
	@include('contacts._form')
	{{Form::close()}}
@stop