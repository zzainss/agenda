<?php

class UsersController extends BaseController
{
	public function index()
	{
		$users = User::all();
		return View::make('users.index', array('users' => $users));
	}

	public function create()
	{
		return View::make('users.create', array('user' => new User()));
	}

	public function store()
	{
		$user = new User();
		$user->name = Input::get('name');
		$user->username = Input::get('username');
		$user->email = Input::get('email');
		$user->password = Input::get('password');
		$user->password_confirmation = Input::get('password_confirmation');

		if($user->save())
		{
			return Redirect::route('users.show', array($user->id));
		}
		else
		{
			return Redirect::route('users.create')->withErrors($user->errors());
		}
	}

	public function show($id)
	{
		$user = User::find($id);
		//$user->contacts = $user->filtercontactsByCriteria('text', 'NOT LIKE', 'Prueba%');
		$user->contacts = $user->getNcontactsOrdered(10, 'created_at', 'DESC');

		if($user)
		{
			return View::make('users.show', ['user' => $user]);
		}
		else
		{
			Throw new NotFoundHttpException;
		}	
	}

	public function edit($id)
	{
		$user = User::find($id);

		if($user)
		{
			return View::make('users.edit', array('user' => $user));
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	public function update($id)
	{
		$user = User::find($id);

		if($user)
		{
			$user->name = Input::get('name');
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->password = Input::get('password');
			$user->password_confirmation = Input::get('password_confirmation');
			$user->bio = Input::get('bio');

			if($user->updateUniques())
			{
				return Redirect::route('users.show', array($user->id));
			}
			else
			{
				return Redirect::route('users.edit', array($id))->withErrors($user->errors());
			}
		}
		else
		{
			Throw new NotFoundHttpException;	
		}
	}

	public function delete($id)
	{
		$user = User::find($id);

		if($user)
		{
			if($user->delete())
			{
				return Redirect::route('users.index');
			}
			else
			{
				App::abort(500);
			}
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	public function login()
	{
		$error = Session::get('error');
		return View::make('users.login')->with('error', $error);
	}

	public function auth()
	{
		$auth_data = ['email' => Input::get('email'), 'password' => Input::get('password')];

		if(Auth::attempt($auth_data))
		{
			return Redirect::intended( route('users.main') );
		}
		else
		{
			return Redirect::route('users.login')->with('error', Lang::get('messages.wrong_credentials'))->withInput();
		}
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::route('users.login')->with('error', 'Has cerrado sesión exitosamente');
	}

	public function test()
	{
		$user = User::find(1);

		//$contacts = contact::all();

		$contacts = contact::where('created_at', '<', '2014-10-31 23:59:59')->orderBy('text', 'asc')->get();

		$i = 0;

		foreach($contacts as $contact)
		{
			if(! $user->likes($contact->id))
			{
				$user->likes()->attach($contact->id);
				$i++;
			}
		}

		echo "Al usuario $user->name le gustaron $i publicaciones";

		echo '<pre>';
		var_dump($user);
	}

	public function test2()
	{
		$contact = contact::find(1);

		$users = User::all();

		$i = 0;

		foreach($users as $user)
		{
			$contact->likedBy()->attach($user->id);
			$i++;
		}
		echo "El contact \" $contact->text \" le gustó a $i personas";
	}

	public function test3()
	{
		$contact = contact::with('likedBy', 'likedBy.likes')->find(1);
		//$contact = contact::find(1);

		foreach($contact->likedBy as $user)
		{
			echo "<p>Al usuario {$user->name} le gustan {$user->likes->count()} publicaciones </p>";
		}
	}
}