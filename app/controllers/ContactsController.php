<?php

class ContactsController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function index()
	{
		//$contacts = contact::orderBy('created_at', 'desc')->get();
		//$contacts = contact::with('user')->where('user_id', '=', Auth::user()->id)->orderBy('text', 'desc')->get();
		$contacts = contact::with('user')->orderBy('created_at', 'desc')->get();

		return View::make('contacts.index', ['contacts' => $contacts]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /contacts/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$contact = new contact();
		return View::make('contacts.create')->with('contact', $contact);
	}


	public function store()
	{
		//Si el usuario está logeado
		if(Auth::check())
		{
			//Guardamos el contact
			$contact = new contact();
			$contact->name = Input::get('name');
			$contact->last_name = Input::get('last_name');
			$contact->email = Input::get('email');
			$contact->address = Input::get('address');
			$contact->user_id = Auth::user()->id;

			if($contact->save())
			{
				return Redirect::route('contacts.show', [$contact->id]);
			}
			else
			{
				return Redirect::route('contacts.create')->withInput()->withErrors($contact->errors());
			}
		}
		else
		{
			//Redireccionamos hacia la pantalla de login
			return Redirect::route('users.login')->withError('Debes iniciar sesión.');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /contacts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$contact = contact::find($id);

		if($contact)
		{
			return View::make('contacts.show', ['contact' => $contact]);
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /contacts/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contact = Contact::find($id);

		if($contact)
		{
			return View::make('contacts.edit', array('contact' => $contact));
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /contacts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$contacts = Contact::find($id);

		if($contacts)
		{
			$contacts->name = Input::get('name');
			$contacts->last_name = Input::get('last_name');
			$contacts->email = Input::get('email');
			$contacts->address = Input::get('address');

			if($contacts->updateUniques())
			{
				return Redirect::route('contacts.show', array($contacts->id));
			}
			else
			{
				return Redirect::route('contacts.edit', array($id))->withErrors($contacts->errors());
			}
		}
		else
		{
			Throw new NotFoundHttpException;	
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /contacts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}