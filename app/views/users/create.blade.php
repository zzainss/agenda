@extends('theme')

@section('title') Nuevo Usuario @stop

@section('header_title') Nuevo Usuario @stop

@section('content')
	{{BootForm::open()->post()->action(route('users.store'))}}
	@include('users._form')
	{{BootForm::close()}}
@stop