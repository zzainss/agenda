<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;

class User extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'users';

	protected $primaryKey = 'id';

	/**
	 * Ardent validation rules
	 *
	 * @var array
	 */
	public static $rules = array(
		'name'					=> 'required',
		'username'				=> 'required|unique:users,username',
		'email'					=> 'required|email|unique:users,email',
		'password'				=> 'required|min:8|confirmed',
		'password_confirmation' => 'same:password',
	);

	/**
	 * Ardent: purge redundant attributes
	 *
	 * @var boolean
	 */
	public $autoPurgeRedundantAttributes = true;

  public static $passwordAttributes  = array('password');
  
  public $autoHashPasswordAttributes = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function contacts()
	{
		return $this->hasMany('contact');
	}

	public function telephone()
	{
		return $this->belongsToMany('contact');
	}

	/**
	 * Regresa una lista de $n contacts ordenados por un campo
	 *
	 * @param int $n Cantidad de contacts a cargar
	 * @param string $orderBy Columna en base a la cual ordenar
	 * @param string $order Sentido en que se ordena ['asc'|'desc']
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getNcontactsOrdered($n, $orderBy, $order = 'asc')
	{
		return $this->contacts()->orderBy($orderBy, $order)->take($n)->get();
	}

	/**
	 * filtercontactsByCriteria
	 *
	 * @param string $column Columna en base a la cual filtrar
	 * @param string $operator Operador de la sentencia WHERE
	 * @param mixed $value Valor para usar en la sentencia WHERE
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function filtercontactsByCriteria($column, $operator, $value)
	{
		return $this->contacts()->where($column, $operator, $value)->get();
	}

	public function usertelephone($contact_id)
	{
		$contact = $user->telephone()->where('contact_id', '=', $contact->id)->first();

		return ($contact) ? true : false;
	}
}
