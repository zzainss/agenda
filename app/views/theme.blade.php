<!DOCTYPE html>
<html lang="en">
<head>	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Agenda de Contactos - @yield('title')</title>

	<link href="{{ asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">
	<link href="{{ asset('css/bootstrap-example.css')}}" rel="stylesheet" media="screen">
	<link href="{{ asset('css/styles.css')}}" rel="stylesheet" media="screen">
</head>
<body>
	@section('navbar')
  <div class="navbar-wrapper {{ ( isset($class) && ! empty($class) ) ? $class : null }}">
    <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
    <div class="container">
      <div class="navbar navbar-inverse">
        <div class="navbar-inner">
          <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="{{route('users.main')}}">Contact</a>
          <div class="nav-collapse collapse">
                <ul class="nav">
                	@if(Auth::guest())
                  <li><a href="{{route('users.create')}}">Registro</a></li>
                  <li><a href="{{route('users.login')}}">Inicio de Sesión</a></li>
                	@else
                  <li><a href="{{route('contacts.index')}}">Mis Contactos</a></li>
                  |
									<li><a href="{{route('users.show', ['id' => Auth::user()->id])}}">Mi Cuenta</a></li>
									<li><a href="{{route('users.edit', ['id' => Auth::user()->id])}}">Editar perfil</a></li>
									<li><a href="{{route('users.logout')}}">Cerrar Sesión</a></li>
                	@endif
                </ul>
          </div><!--/.nav-collapse -->
        </div><!-- /.navbar-inner -->
      </div><!-- /.navbar -->

    </div> <!-- /.container -->
  </div><!-- /.navbar-wrapper -->
	@show

	<header class="container">
	@section('header')
		<h1>@yield('header_title')</h1>
	@show
	</header>

	<div id="main" class="container">
		@yield('content')
	</div>

  <footer class="container">
    <p class="pull-right"><a href="#">Back to top</a></p>
  </footer>
	
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>	
</body>
</html>