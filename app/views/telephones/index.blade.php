@extends('theme')

@section('title') Lista de contacts @stop

@section('header')
	@parent
	<h2>Bienvenido</h2>
@stop

@section('header_title') Lista de contacts @stop

@section('content')
<style>
	th,td{
		padding: 3px;
		border: solid #000 1px;
	}
</style>
	@if(! $telephones->isEmpty())
		<table style="border">
			<thead>
				<tr>
					<th>id</th>
					<th>publicado por</th>
					<th>Nombre</th>
					<th>email</th>
					<th>address</th>
					<th>created_at</th>
					<th>updated_at</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($telephone as $telephone)
				<tr>
					<td>{{$telephones->id}}</td>
					<td>{{$telephones->user->name}}</td>
					<td>{{$telephones->contacts->name}}</td>
					<td>{{$telephones->contacts->last_name}}</td>
					<td>{{$telephones->label}}</td>
					<td>{{$telephones->telephone_number}}</td>
					<td>{{$telephones->created_at}}</td>
					<td>{{$telephones->updated_at}}</td>					
					<td>
						<a href="{{route('contacts.show', array($contacts->id))}}">Ver</a>
						<a href="{{route('contacts.edit', array($contacts->id))}}">Editar</a>
					</td>					
				</tr>
			@endforeach
			</tbody>
		</table>
	@else 
		<p>No hay contacts</p>
	@endif
	<a href="{{route('telephones.create')}}">+ Nuevo contacts</a>
@stop