<?php

use LaravelBook\Ardent\Ardent;

class Contact extends Ardent {
	
	/**
	 * Ardent validation rules
	 *
	 * @var array
	 */
	public static $rules = array(
		'user_id'	=> 'required|exists:users,id',
		'name'		=> 'required',
		'last_name'	=> '',
		'email'		=> 'email',
		'address'	=> '',
	);

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function likedBy()
	{
		return $this->belongsToMany('User');
	}
}