@extends('theme')

@section('title') Lista de contacts @stop

@section('header')
	@parent
	<h2>Bienvenido</h2>
@stop

@section('header_title') Lista de contacts @stop

@section('content')
<style>
	th,td{
		padding: 3px;
		border: solid #000 1px;
	}
</style>
	@if(! $contacts->isEmpty())
		<table style="border">
			<thead>
				<tr>
					<th>id</th>
					<th>Propietario</th>
					<th>Nombre</th>
					<th>email</th>
					<th>address</th>
					<th>Creado</th>
					<th>Modificado</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($contacts as $contacts)
				<tr>
					<td>{{$contacts->id}}</td>
					<td>{{$contacts->user->name}}</td>
					<td>{{$contacts->name}}, {{$contacts->last_name}}</td>
					<td>{{$contacts->email}}</td>
					<td>{{$contacts->address}}</td>
					<td>{{$contacts->created_at}}</td>
					<td>{{$contacts->updated_at}}</td>					
					<td>
						<a href="{{route('contacts.show', array($contacts->id))}}" class="btn">Ver</a>
						<a href="{{route('contacts.edit', array($contacts->id))}}" class="btn">Editar</a>
						<a href="{{route('telephones.create')}}" class="btn" value="{{$contacts->id}}">+Numero</a>
					</td>					
				</tr>
			@endforeach
			</tbody>
		</table>
	@else 
		<p>No hay contacts</p>
	@endif
	<a href="{{route('contacts.create')}}">+ Nuevo contacts</a>
@stop