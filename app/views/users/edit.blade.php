@extends('theme')

@section('title') Edición de Usuario {{$user->username}} @stop

@section('header_title') Editando perfil de {{$user->username}} @stop

@section('content')
	{{Form::open(array('route' => array('users.update', $user->id), 'method' => 'PUT'))}}
	@include('users._form')
	{{Form::close()}}
@stop