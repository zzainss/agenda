@extends('theme')

@section('title') {{Lang::get('messages.welcome', ['name' => $user->name])}} @stop

@section('header')
	@parent
	<h2></h2>
	<a href="{{route('users.logout')}}">Cerrar sesión</a>
@stop

@section('header_title') {{Lang::get('messages.welcome', ['name' => $user->name])}} @stop

@section('content')
	<p>Timeline</p>
@stop