	<div class="text-info">
	 @if($errors->has('name'))
		@foreach($errors->get('name') as $error):
		{{Form::label('name', $error)}}
		@endforeach
	@endif
	@if($errors->has('last_name'))
		@foreach($errors->get('last_name') as $error):
		{{Form::label('last_name', $error)}}
		@endforeach
	@endif
	@if($errors->has('email'))
		@foreach($errors->get('email') as $error):
		{{Form::label('email', $error)}}
		@endforeach
	@endif
	@if($errors->has('address'))
		@foreach($errors->get('address') as $error):
		{{Form::label('address', $error)}}
		@endforeach
	@endif
	</div>

<fieldset>
	{{Form::text('name', $contact->name, array('placeholder' => 'Nombre'))}}
</fieldset>
<fieldset>
	{{Form::text('last_name', $contact->last_name, array('placeholder' => 'Apellido'))}}
</fieldset>
<fieldset>
	{{Form::text('email', $contact->email, array('placeholder' => 'corre@pagina.com'))}}
</fieldset>
<fieldset>
	{{Form::text('address', $contact->address, array('placeholder' => 'Direccion'))}}
</fieldset>
<fieldset>
	{{Form::submit()}}
</fieldset>