@extends('theme')

@section('title') Perfil de {{$user->name}} @stop

@section('header')
	@parent
	<h2>Bienvenido</h2>
@stop

@section('header_title') Perfil de {{$user->name}} @stop

@section('content')
	<section>
		<article>
			<p><b>Nombre:</b> {{$user->name}}</p>
			<p><b>Nombre de usuario:</b> {{$user->username}}</p>
			<p><b>Email:</b> {{$user->email}}</p>
			<p><b>Fecha de registro:</b> {{$user->created_at}}</p>
			<p><b>Última modificación:</b> {{$user->updated_at}}</p>
		</article>
		<aside>
			@if(! $user->contacts->isEmpty())
				@foreach($user->contacts as $post)
					<span class="text">{{$post->name}} {{$post->last_name}}</span>
					<span class="date" style="font-size: 10px;">Publicado el: {{$post->created_at}}</span><br>			
				@endforeach
				<a href="{{route('contacts.index')}}">Lista de Contactos</a>
			@else
			<p>El usuario no ha publicado aún.</p>
			@endif
		</aside>

	</section>

@stop