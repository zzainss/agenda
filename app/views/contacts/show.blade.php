@extends('theme')

@section('title') {{{$contact->name}}} {{{$contact->last_name}}} @stop

@section('header')
	@parent
	<h2>Contacto de: <a href="{{route('users.show', ['id' => $contact->user->id])}}">{{{$contact->user->name}}}</a></h2>
	<h3>El día: {{$contact->created_at}}</h3>
@stop

@section('header_title') contact Nro. {{$contact->id}} @stop

@section('content')
<article>
	<p style="font-size: 1em">{{{$contact->name}}}</p>
	<p style="font-size: 1em">{{{$contact->last_name}}}</p>
	<p style="font-size: 1em">{{{$contact->email}}}</p>
	<p style="font-size: 1em">{{{$contact->address}}}</p>
</article>
@stop