<?php

use LaravelBook\Ardent\Ardent;

class Telephones extends Ardent {
	
	/**
	 * Ardent validation rules
	 *
	 * @var array
	 */
	public static $rules = array(
		'user_id'			=> 'required|exists:users,id',
		'label'				=> 'required|min:3|max:4',
		'telephone_number'	=> 'required|min:7|max:7'
	);

	public function contact()
	{
		return $this->belongsTo('Contact');
	}

}