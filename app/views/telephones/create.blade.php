@extends('theme')

@section('title') Nuevo Contacto @stop

@section('header_title') Nuevo Contacto @stop

@section('content')
	{{Form::open(array('route' => 'telephones.store', 'method' => 'POST'))}}
	@include('telephones._form')
	{{Form::close()}}
@stop