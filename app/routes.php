<?php
Route::get('/', array('as' => 'users.main', 'uses' => 'HomeController@main'));

//Route::resource('/users', 'UsersController');
//Rutas de recurso usuario
Route::get('/users', array('as' => 'users.index', 'uses' => 'UsersController@index'));
Route::get('/users/create', array('as' => 'users.create', 'uses' => 'UsersController@create'));
Route::post('/users', array('as' => 'users.store', 'uses' => 'UsersController@store'));
Route::get('/users/{id}', array('as' => 'users.show', 'uses' => 'UsersController@show'))->where('id', '[0-9]+');
Route::get('/users/{id}/edit', array('as' => 'users.edit', 'uses' => 'UsersController@edit'))->where('id', '[0-9]+');
Route::put('/users/{id}', array('as' => 'users.update', 'uses' => 'UsersController@update'))->where('id', '[0-9]+');
Route::delete('/users/{id}', array('as' => 'users.delete', 'uses' => 'UsersController@delete'))->where('id', '[0-9]+');
Route::get('/users/login', array('as' => 'users.login', 'uses' => 'UsersController@login'));
Route::post('/users/auth', array('as' => 'users.auth', 'uses' => 'UsersController@auth'));
Route::get('/users/logout', array('as' => 'users.logout', 'uses' => 'UsersController@logout'));


Route::resource('contacts', 'ContactsController');

Route::post('/telephones/create', array('as' => 'telephones.create', 'uses' => 'TelephonesController@create'));
Route::post('/telephones', array('as' => 'telephones.store', 'uses' => 'TelephonesController@store'));

