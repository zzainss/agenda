<?php

class TelephonesController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function index()
	{
		$telephones = telephones::with('telephones')->orderBy('created_at', 'desc')->get();

		return View::make('telephones.index', ['telephones' => $telephones]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /telephones/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$telephones = new telephones();
		return View::make('telephones.create')->with('telephones', $telephones);
	}


	public function store()
	{
		//Si el usuario está logeado
		if(Auth::check())
		{
			//Guardamos el contact
			$telephones = new telephones();
			$telephones->label = Input::get('label');
			$telephones->telephones = Input::get('telephones');
			$telephones->contact_id = Auth::contact()->id;

			if($telephones->save())
			{
				return Redirect::route('telephones.show', [$telephones->id]);
			}
			else
			{
				return Redirect::route('telephones.create')->withInput()->withErrors($telephones->errors());
			}
		}
		else
		{
			//Redireccionamos hacia la pantalla de login
			return Redirect::route('users.login')->withError('Debes iniciar sesión.');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /telephones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$telephones = telephones::find($id);

		if($telephones)
		{
			return View::make('telephones.show', ['contact' => $contact]);
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /telephones/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$contact = Contact::find($id);

		if($contact)
		{
			return View::make('telephones.edit', array('contact' => $contact));
		}
		else
		{
			Throw new NotFoundHttpException;
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /telephones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$telephones = Contact::find($id);

		if($telephones)
		{
			$telephones->name = Input::get('name');
			$telephones->last_name = Input::get('last_name');
			$telephones->email = Input::get('email');
			$telephones->addres = Input::get('addres');

			if($telephones->updateUniques())
			{
				return Redirect::route('telephones.show', array($telephones->id));
			}
			else
			{
				return Redirect::route('telephones.edit', array($id))->withErrors($telephones->errors());
			}
		}
		else
		{
			Throw new NotFoundHttpException;	
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /telephones/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}